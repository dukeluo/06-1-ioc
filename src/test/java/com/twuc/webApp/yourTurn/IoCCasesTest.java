package com.twuc.webApp.yourTurn;

import com.twuc.unknown.OutScanningScope;
import com.twuc.webApp.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class IoCCasesTest {

    // 2.1.1
    @Test
    void should_create_object_without_dependency() {
        GenericApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        WithoutDependency bean = context.getBean(WithoutDependency.class);

        assertNotNull(bean);
        assertSame(WithoutDependency.class, bean.getClass());
    }

    // 2.1.2
    @Test
    void should_create_object_with_dependency() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        WithDependency bean = context.getBean(WithDependency.class);

        assertNotNull(bean);
        assertSame(WithDependency.class, bean.getClass());
        assertSame(Dependent.class, bean.getDependency().getClass());
    }

    // 2.1.3
    @Test
    void should_throw_exception_when_the_class_out_of_scanning_range() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        assertThrows(NoSuchBeanDefinitionException.class, () -> {
            OutScanningScope bean = context.getBean(OutScanningScope.class);
        });
    }

    // 2.1.4
    @Test
    void should_get_object_by_the_implemented_interface() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        Interface bean = context.getBean(Interface.class);

        assertNotNull(bean);
        assertSame(InterfaceImpl.class, bean.getClass());
    }

    // 2.1.5
    @Test
    void should_get_object_by_the_implemented_interface_using_factory() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SimpleInterface bean = context.getBean(SimpleInterface.class);

        assertNotNull(bean);
        assertSame(SimpleObject.class, bean.getClass());
        assertEquals("O_o", bean.getSimpleDependent().getName());
    }

    // 2.2.1
    @Test
    void should_create_object_with_the_first_constructor() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        MultipleConstructor bean = context.getBean(MultipleConstructor.class);

        assertNotNull(bean);
        assertSame(MultipleConstructor.class, bean.getClass());
    }
    
    // 2.2.2
    @Test
    void should_call_constructor_and_autowired() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);

        assertNotNull(bean);
        assertSame(WithAutowiredMethod.class, bean.getClass());
        assertEquals(Arrays.asList("WithAutowiredMethod()", "initialize()"), bean.getLogs());
    }
    
    // 2.2.3
    @Test
    void should_get_multiple_instance() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);

        assertNotNull(beans);
        assertEquals(3, beans.size());
        assertArrayEquals(new String[] {
                "com.twuc.webApp.ImplementationA", "com.twuc.webApp.ImplementationB", "com.twuc.webApp.ImplementationC"
        }, beans.values().stream().map(i -> i.getClass().getName()).toArray(String[]::new));
    }
}
